use bevy::prelude::*;

use chess::board::Chessboard;

mod chess;
mod ui;

/// This example illustrates the various features of Bevy UI.
fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "chess".to_string(),
            width: 1200.,
            height: 900.,
            vsync: true,
            ..Default::default()
        })
        .insert_resource(Chessboard::new())
        .add_plugins(DefaultPlugins)
        .add_startup_system(ui::setup)
        .add_system(ui::action::piece_click_system.label("input"))
        .add_system(ui::action::board_update_system.after("input"))
        .run();
}

