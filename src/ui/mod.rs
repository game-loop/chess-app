
use bevy::prelude::*;

pub mod action;

const h: [f32; 7] = [92.0, 165.0, 238.0, 411.0, 575.0, 650.0, 725.0];
const v: [f32; 7] = [91.0, 165.0, 235.0, 408.0, 590.0, 662.0, 733.0];

#[derive(Component, Debug)]
pub struct PieceId{
    id: u8,
}

pub fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    // ui camera
    commands.spawn_bundle(UiCameraBundle::default());

    // root node
    commands.spawn_bundle(NodeBundle {
        style: Style {
            size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
            justify_content: JustifyContent::SpaceBetween,
            ..Default::default()
        },
        color: Color::GRAY.into(),
        ..Default::default()
    }).with_children(|parent|{
        parent.spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(900.0), Val::Px(900.0)),
                border: Rect::all(Val::Px(2.0)),
                ..Default::default()
            },
            color: Color::rgb(0.65, 0.65, 0.65).into(),
            ..Default::default()
        }).with_children(|parent| {
            parent.spawn_bundle(ImageBundle{
                style: Style {
                    size: Size::new(Val::Px(800.0), Val::Px(800.0)),
                    margin: Rect::all(Val::Px(50.0)),
                    ..Default::default()
                },
                image: asset_server.load("chessboard.png").into(),
                ..Default::default()
            });
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[0]),
                        top: Val::Px(v[0]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 0});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[0]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 1});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[6]),
                        top: Val::Px(v[0]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 2});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[6]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 3});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[6]),
                        top: Val::Px(v[6]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 4});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[6]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 5});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[0]),
                        top: Val::Px(v[6]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 6});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[0]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 7});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[1]),
                        top: Val::Px(v[1]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 8});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[1]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 9});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[5]),
                        top: Val::Px(v[1]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 10});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[5]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 11});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[5]),
                        top: Val::Px(v[5]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 12});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[5]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 13});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[1]),
                        top: Val::Px(v[5]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 14});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[1]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 15});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[2]),
                        top: Val::Px(v[2]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 16});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[2]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 17});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[4]),
                        top: Val::Px(v[2]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 18});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[4]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 19});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[4]),
                        top: Val::Px(v[4]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 20});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[3]),
                        top: Val::Px(v[4]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 21});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[2]),
                        top: Val::Px(v[4]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 22});
            parent.spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Px(70.0), Val::Px(70.0)),
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(h[2]),
                        top: Val::Px(v[3]),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                image: asset_server.load("empty.png").into(),
                ..Default::default()
            }).insert(PieceId{id: 23});
        });
        parent.spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(300.0), Val::Px(900.0)),
                border: Rect::all(Val::Px(2.0)),
                margin: Rect::all(Val::Px(2.0)),
                padding: Rect::all(Val::Px(2.0)),
                ..Default::default()
            },
            color: Color::rgb(0.65, 0.65, 0.65).into(),
            ..Default::default()
        });
    });
}