
use std::borrow::BorrowMut;
use std::rc::{Rc, Weak};
use std::cell::RefCell;

pub mod board;
pub mod player;
// mod pin;
// mod rc;
// pub mod arc;

#[derive(Debug, Clone)]
pub enum PieceColor {
    Black,
    White
}

#[derive(Debug, Clone)]
pub enum PointState {
    Kill,
    Piece(PieceColor)
}

#[derive(Debug)]
pub struct Point {
    id: u8,
    piece: Option<PointState>,
    h: u8,
    v: u8,
}

impl Point {
    pub fn new(id: u8, h: u8, v: u8,) -> Point {
        Point{
            id,
            piece: None,
            h,
            v,
        }
    }
}

#[derive(Debug)]
pub struct Edge {
    left: u8,
    right: u8,
    center: u8,
}

impl Edge {
    pub fn new(left: u8, center: u8, right: u8) -> Edge {
        Edge {
            left,
            right,
            center
        }
    }
}

pub enum ChessState {
    Put,
    Move,
    Kill,
    Over,
}