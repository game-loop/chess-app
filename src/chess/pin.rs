
use std::marker::PhantomPinned;
use std::pin::Pin;
use std::ptr::NonNull;

use std::default::Default;
use crate::chess::{PieceColor, PointState};

#[derive(Debug)]
pub struct Point {
    id: u8,
    piece: Option<PointState>,
    v: NonNull<Edge>,
    h: NonNull<Edge>,
    _p: PhantomPinned,
}

#[derive(Debug)]
pub struct Edge {
    point: Vec<Point>,
}