
use crate::chess::PieceColor;

pub struct Player {
    count: u8,
    color: PieceColor,
}

impl Player {
    pub fn new(color: PieceColor) -> Player {
        Player{
            count: 9,
            color,
        }
    }

    pub fn color(&self) -> PieceColor {
        self.color.clone()
    }
}