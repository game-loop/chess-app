use std::sync::Arc;

use crate::chess::player::Player;
use crate::chess::{ChessState, Edge, PieceColor, Point, PointState};

use bevy::prelude::*;

pub struct Chessboard {
    state: ChessState,
    points: [Point; 24],
    lines: [Edge; 16],
    current: Player,
    opponent: Player,
}

impl Chessboard {

    pub fn point_state(&self, id: u8) -> Option<PointState> {
        let id = id as usize;
        if id  > self.points.len() {
            None
        } else {
            self.points[id].piece.clone()
        }
    }

    pub fn update(&mut self, id: u8) {
        let id = id as usize;
        let color = self.current.color();
        self.points[id].piece = Some(PointState::Piece(color.clone()));
        std::mem::swap(&mut self.current, &mut self.opponent);
        // info!("put piece, id {:?}, color {:?}", id, color)
        // info!("{:#?}", self.points);
    }

    pub fn new() -> Chessboard {
        let mut p00 = Point::new(0, 0, 3);
        let mut p01 = Point::new(1, 0, 12);
        let mut p02 = Point::new(2, 0, 1);
        let mut p03 = Point::new(3, 1, 13);
        let mut p04 = Point::new(4, 1, 2);
        let mut p05 = Point::new(5, 2, 14);
        let mut p06 = Point::new(6, 2, 3);
        let mut p07 = Point::new(7, 2, 15);
        let mut p08 = Point::new(8, 4, 7);
        let mut p09 = Point::new(9, 4, 12);
        let mut p10 = Point::new(10, 5, 4);
        let mut p11 = Point::new(11, 5, 13);
        let mut p12 = Point::new(12, 6, 4);
        let mut p13 = Point::new(13, 6, 14);
        let mut p14 = Point::new(14, 7, 6);
        let mut p15 = Point::new(15, 7, 15);
        let mut p16 = Point::new(16, 8, 7);
        let mut p17 = Point::new(17, 8, 12);
        let mut p18 = Point::new(18, 9, 8);
        let mut p19 = Point::new(19, 9, 13);
        let mut p20 = Point::new(20, 10, 9);
        let mut p21 = Point::new(21, 10, 14);
        let mut p22 = Point::new(22, 11, 10);
        let mut p23 = Point::new(23, 11, 15);

        let l01 = Edge::new(0, 1, 2);
        let l02 = Edge::new(2, 3, 4);
        let l03 = Edge::new(4, 5, 6);
        let l04 = Edge::new(6, 7, 0);
        let l05 = Edge::new(8, 9, 10);
        let l06 = Edge::new(10, 11, 12);
        let l07 = Edge::new(12, 13, 14);
        let l08 = Edge::new(14, 15, 8);
        let l09 = Edge::new(16, 17, 18);
        let l10 = Edge::new(18, 19, 20);
        let l11 = Edge::new(20, 21, 22);
        let l12 = Edge::new(22, 23, 16);
        let l13 = Edge::new(1, 9, 17);
        let l14 = Edge::new(3, 11, 19);
        let l15 = Edge::new(5, 13, 21);
        let l16 = Edge::new(7, 15, 23);

        Chessboard {
            state: ChessState::Put,
            points: [
                p00, p01, p02, p03, p04, p05, p06, p07,
                p08, p09, p10, p11, p12, p13, p14, p15,
                p16, p17, p18, p19, p20, p21, p22, p23,
            ],
            lines: [
                l01, l02, l03, l04, l05, l06, l07, l08, l09, l10, l11, l12, l13, l14, l15, l16,
            ],
            // current: Arc::new(Player::new(PieceColor::Black)),
            // opponent: Arc::new(Player::new(PieceColor::White)),
            current: Player::new(PieceColor::Black),
            opponent: Player::new(PieceColor::White),
        }
    }
}
